import axios from "axios";

const axiosClient = axios.create({
  baseURL: process.env.REACT_APP_DATABASE_URL,
  timeout: 1000,
  headers: { ContentType: "application/json" },
});

export default axiosClient;
