import styles from "./style.module.scss";
import TodoList from "../TodoList";
import Filters from "../Filters";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { fetchTodos } from "../../redux/todo/todosSlice";

function Form() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchTodos());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className={styles.container}>
      <div className={styles.title}>
        <h2>Todo App</h2>
      </div>
      <Filters />
      <hr />
      <TodoList />
    </div>
  );
}

export default Form;
