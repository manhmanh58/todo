import { Col, Row, Input, Typography, Radio } from "antd";
import { useState, useCallback } from "react";
import { useDispatch } from "react-redux";
import filtersSlice from "../../redux/filter/filtersSlice";
import debounce from "lodash/debounce";

const { Search } = Input;

export default function Filters() {
  const dispatch = useDispatch();

  const [searchText, setSearchText] = useState("");
  const [filterStatus, setFilterStatus] = useState("All");

  const handleSearchTextChange = (value) => {
    dispatch(
      filtersSlice.actions.searchFilterChange(value.toLocaleLowerCase())
    );
  };

  const debounceFn = useCallback(debounce(handleSearchTextChange, 600), []);

  const onChange = (e) => {
    setSearchText(e.target.value);
    debounceFn(e.target.value);
  };

  const handleStatusChange = (e) => {
    setFilterStatus(e.target.value);
    dispatch(filtersSlice.actions.statusFilterChange(e.target.value));
  };

  return (
    <Row justify="center">
      <Col span={24}>
        <Typography.Paragraph
          style={{ fontWeight: "bold", marginBottom: 3, marginTop: 10 }}
        >
          Search
        </Typography.Paragraph>
        <Search
          placeholder="input search text"
          value={searchText}
          onChange={(e) => onChange(e)}
        />
      </Col>
      <Col sm={24}>
        <Typography.Paragraph
          style={{ fontWeight: "bold", marginBottom: 3, marginTop: 10 }}
        >
          Filter By Status
        </Typography.Paragraph>
        <Radio.Group value={filterStatus} onChange={handleStatusChange}>
          <Radio value="All">All</Radio>
          <Radio value="Completed">Completed</Radio>
          <Radio value="Todo">To do</Radio>
        </Radio.Group>
      </Col>
    </Row>
  );
}
