import { Col, Row, Input, Button } from "antd";
import Todo from "../Todo";
import { useDispatch, useSelector } from "react-redux";
import { v4 as uuidv4 } from "uuid";
import { useState, useRef, useEffect } from "react";
import { todosRemainingSelector } from "../../redux/selectors";
import { addNewTodo } from "../../redux/todo/todosSlice";
import { ToastContainer, toast } from "react-toastify";

export default function TodoList() {
  const inputRef = useRef(null);
  useEffect(() => {
    inputRef.current.focus();
  }, []);

  const [todoName, setTodoName] = useState("");

  const todoList = useSelector(todosRemainingSelector);

  const dispatch = useDispatch();

  const handleAddButtonClick = () => {
    const todo = todoName.trim();
    if (todo === "") {
      toast.error("Please type something!", {
        position: toast.POSITION.TOP_CENTER,
      });
      inputRef.current.focus();
      return;
    }
    dispatch(
      addNewTodo({
        id: uuidv4(),
        name: todoName,
        completed: false,
      })
    );
    toast.success("Add todo successfully!", {
      position: toast.POSITION.TOP_CENTER,
    });
    inputRef.current.focus();
    setTodoName("");
  };
  const handleInputChange = (e) => {
    setTodoName(e.target.value);
  };
  return (
    <Row style={{ height: "calc(100% - 40px)" }}>
      <Col span={24} style={{ height: "calc(100% - 40px)", overflowY: "auto" }}>
        {todoList.map((todo) => (
          <Todo
            key={todo.id}
            id={todo.id}
            name={todo.name}
            completed={todo.completed}
          />
        ))}
      </Col>
      <Col span={24}>
        <Input.Group style={{ display: "flex" }} compact>
          <Input
            value={todoName}
            ref={inputRef}
            onKeyPress={(e) => {
              if (e.key === "Enter") {
                handleAddButtonClick();
              }
            }}
            onChange={handleInputChange}
          />
          <Button type="primary" onClick={handleAddButtonClick}>
            Add
          </Button>
        </Input.Group>
      </Col>
      <ToastContainer limit={3} />
    </Row>
  );
}
