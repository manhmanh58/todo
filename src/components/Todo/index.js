import styles from "./style.module.scss";
import { Row, Checkbox, Input, Button, Modal } from "antd";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { FiEdit } from "react-icons/fi";
import todoListSlice, { updateTodo } from "../../redux/todo/todosSlice";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { ToastContainer, toast } from "react-toastify";

export default function Todo({ name, completed, id }) {
  const dispatch = useDispatch();
  const [editTodoName, setEditTodoName] = useState(name);

  const [isEditing, setEditing] = useState(false);
  const [checked, setChecked] = useState(completed);

  const toggleCheckbox = () => {
    setChecked(!checked);
    dispatch(todoListSlice.actions.toggleTodoStatus(id));
  };

  const onEditToggle = () => {
    if (checked) {
      setEditing(false);
    } else {
      setEditing(true);
    }
  };

  const removeTodo = () => {
    dispatch(
      todoListSlice.actions.removeTodos({
        id: id,
      })
    );
    toast.success("Delete todo successfully!", {
      position: toast.POSITION.TOP_CENTER,
    });
  };

  const handleInputChange = (e) => {
    setEditTodoName(e.target.value);
  };

  const handleEditButtonClick = () => {
    if (editTodoName === "" || /^\s*$/.test(editTodoName)) {
      toast.error("Please type something!", {
        position: toast.POSITION.TOP_CENTER,
      });
      return;
    }
    dispatch(
      updateTodo({
        name: editTodoName,
        id: id,
      })
    );
    toast.success("Update todo successfully!", {
      position: toast.POSITION.TOP_CENTER,
    });
    setEditing(false);
  };

  const handleCancel = () => {
    setEditing(false);
  };

  const { confirm } = Modal;

  const showConfirm = () => {
    confirm({
      title: "Do you Want to delete these items?",
      icon: <ExclamationCircleOutlined />,
      content: "Some descriptions",
      onOk() {
        removeTodo();
      },
    });
  };

  return isEditing ? (
    <Row
      justify="space-between"
      style={{
        marginBottom: 3,
      }}
    >
      {" "}
      <Input.Group style={{ display: "flex" }} compact>
        <Input
          value={editTodoName}
          onKeyPress={(e) => {
            if (e.key === "Enter") {
              handleEditButtonClick();
            }
          }}
          onChange={handleInputChange}
        />
        <Button type="primary" onClick={handleEditButtonClick}>
          Edit
        </Button>
        <Button onClick={handleCancel}>Cancel</Button>
      </Input.Group>
    </Row>
  ) : (
    <Row justify="space-between">
      <Checkbox
        checked={checked}
        onChange={toggleCheckbox}
        style={{
          marginBottom: 3,
          ...(checked ? { opacity: 0.5, textDecoration: "line-through" } : {}),
        }}
      >
        {name}
      </Checkbox>
      <ToastContainer limit={3} />
      <div className={styles.icons}>
        <AiOutlineCloseCircle
          onClick={showConfirm}
          className={styles.delete__icon}
        />
        <FiEdit
          onClick={onEditToggle}
          className={styles.edit__icon}
          style={{
            marginBottom: 3,
            ...(checked ? { opacity: 0.5 } : {}),
          }}
        />
      </div>
    </Row>
  );
}
