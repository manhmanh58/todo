import axiosClient from "../api/axiosCient";

export const todoApi = {
  getAll: async (param) => {
    const url = "/todos";
    return axiosClient.get(url, { param: param });
  },
  get: async (id) => {
    const url = `/todos/${id}`;
    return axiosClient.get(url);
  },
  add: (todo) => {
    const url = "/todos";
    return axiosClient.post(url, todo);
  },

  update: (todo) => {
    const url = `/todos/${todo.id}`;
    return axiosClient.patch(url, todo);
  },
  delete: (id) => {
    const url = `/todos/${id}`;
    return axiosClient.delete(url);
  },
  search: (query) => {
    const url = `/todos/?q=${query}`;
    return axiosClient.get(url);
  },
};
