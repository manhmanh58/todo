import { configureStore } from "@reduxjs/toolkit";
import filtersSlice from "./filter/filtersSlice";
import todosSlice from "./todo/todosSlice";

const store = configureStore({
  reducer: {
    filters: filtersSlice.reducer,
    todoList: todosSlice.reducer,
  },
});

export default store;
