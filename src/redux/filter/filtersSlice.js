import { createSlice } from "@reduxjs/toolkit";
import { todoApi } from "../../services/todoAPI";

export default createSlice({
  name: "filters",
  initialState: {
    search: "",
    status: "All",
  },
  reducers: {
    searchFilterChange: (state, action) => {
      state.search = action.payload;
      todoApi.search(action.payload);
    },
    statusFilterChange: (state, action) => {
      state.status = action.payload;
    },
  },
});
