import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { todoApi } from "../../services/todoAPI";

const todosSlice = createSlice({
  name: "todoList",
  initialState: { status: "idle", todos: [] },
  reducers: {
    toggleTodoStatus: (state, action) => {
      const currentTodo = state.todos.find(
        (todo) => todo.id === action.payload
      );
      if (currentTodo) {
        currentTodo.completed = !currentTodo.completed;
        todoApi.update(currentTodo);
      }
    },
    removeTodos: (state, action) => {
      state.todos = state.todos.filter((item) => item.id !== action.payload.id);
      todoApi.delete(action.payload.id);
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchTodos.pending, (state, action) => {
        state.status = "loading";
      })
      .addCase(fetchTodos.fulfilled, (state, action) => {
        state.todos = action.payload;
        state.status = "idle";
      })
      .addCase(addNewTodo.fulfilled, (state, action) => {
        state.todos.push(action.payload);
      })
      .addCase(updateTodo.fulfilled, (state, action) => {
        const todo = state.todos.find((todo) => todo.id === action.payload.id);
        todo.name = action.payload.name;
      });
  },
});

export const fetchTodos = createAsyncThunk("todos/fetchTodos", async () => {
  const res = await todoApi.getAll();
  const data = res.data;
  return data;
});

export const addNewTodo = createAsyncThunk(
  "todos/addNewTodo",
  async (newTodo) => {
    const res = await todoApi.add(newTodo);
    const data = res.data;
    return data;
  }
);

export const updateTodo = createAsyncThunk(
  "todos/updateTodo",
  async (updatedTodo) => {
    const res = await todoApi.update(updatedTodo);
    const data = res.data;
    return data;
  }
);

export default todosSlice;
